import time

#Написать ф-ию factorial, которая принимает число, а возвращает его факториал

def factorial(x):
    a = 1
    for i in range(1, x + 1):
        a *= i
    return a
#print(factorial(5))

        
for i in range(1, 100 + 1):
    print(str(i) + '!\t' + str(factorial(i)))


print('\n #--------------decorator funct time------------------------------- \n')


def timeit(func):
    def wrap(*args, **kwargs):
        t = time.clock()
        r = func(*args, **kwargs)
        print(func.__name__, time.clock() - t)
        return r
    return wrap

@timeit
def show():
    print("обычная функция")

show()