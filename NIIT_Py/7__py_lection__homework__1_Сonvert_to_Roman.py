class NonValidInput(Exception):
    pass


def Сonvert_to_Roman(data):
    if not isinstance(data, int) or data <= 0 or data > 5000 -1:
        return NonValidInput('NonValidInput')

    else:

        ones = ["", "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX"]
        tens = ["", "X", "XX", "XXX", "XL", "L", "LX", "LXX", "LXXX", "XC"]
        hunds = ["", "C", "CC", "CCC", "CD", "D", "DC", "DCC", "DCCC", "CM"]
        thous = ["", "M", "MM", "MMM", "MMMM"]

        t = thous[data // 1000]
        h = hunds[data // 100 % 10]
        te = tens[data // 10 % 10]
        o = ones[data % 10]

        result = t + h + te + o

    return result


print(Сonvert_to_Roman(155))
