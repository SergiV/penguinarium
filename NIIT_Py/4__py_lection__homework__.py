import os

class WrapStrToFile:
    def __init__(self, filepath):
        self.filepath = filepath
        # здесь инициализируется атрибут filepath, он содержит путь до файла-хранилища

 #   @property
    def content_show(self):
        myfile = open(self.filepath, mode='r', encoding='latin_1')
        for line in myfile:
            print(line.strip())
        # попытка чтения из файла, в случае успеха возвращаем содержимое
        # в случае неудачи возращаем 'Файл еще не существует'
    #
    # @content.setter
    def content_wrate(self, value):
        self.value = value
        myfile = open(self.filepath, mode='w', encoding='latin_1')
        myfile.write(self.value)
    #     # попытка записи в файл указанное содержимого
    #
    # @content.deleter
    def content_delfile(self):
        os.unlink(self.filepath)
    #     # удаляем файл, например, через ф-ию unlink либы os



wstf = WrapStrToFile('mypassword')
wstf.content_show()
wstf.content_wrate('efewf')
wstf.content_show()
wstf.content_wrate('text 2')
wstf.content_show()
wstf.content_delfile()
wstf.content_show()