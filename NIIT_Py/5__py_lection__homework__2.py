from datetime import datetime
import argparse
import os

now = datetime.now()

parser = argparse.ArgumentParser()
parser.add_argument("dirpath", type=str, help="create a directory")
parser.add_argument('-d', '--day', action='store_const', const=str(now.day), help="this day")
parser.add_argument('-m', '--month', action='store_const', const=str(now.month), help="this month")
parser.add_argument('-y', '--year', action='store_const', const=str(now.year), help="this year")
args = parser.parse_args()

date_list = args.year, args.month, args.day

date = []

for a in date_list:
    if a is not None:
        date.append(a)


date = ('-'.join([str(i) for i in date]))


if len(date) > 0:
    date = date
else:
    date = 'unknow'

mydir= '/' + args.dirpath + '/' +date

print("Создание каталога:",mydir)

try:
    os.mkdir(mydir)
except:
    print('невозможно создать каталог' + mydir +': Файл существует')