import os
import datetime

'''
Напишите программу, которая уничтожает файлы и папки по истечении заданного времени.
Вы указываете при запуске программы путь до директории, за которой нашему скрипту необходимо следить.
После запуска программа не должна прекращать работать, пока вы не остановите ее работу с помощью Ctrl+C
(подсказка: для постоянной работы программы необходим вечный цикл, например, "while True:", при нажатии Ctrl+C
автоматически остановится любая программа). Программа следит за объектами внутри указанной при запуске папки и удаляет
их тогда, когда время их существования становится больше одной минуты для файлов и больше двух минуты для папок
(то есть дата создания отличается от текущего момента времени больше чем на одну/две минуту).
Ваш скрипт должен смотреть вглубь указанной папки. Например, если пользователь создаст внутри нее папку,
внутри нее еще одну, а внутри этой какой-то файл, то этот файл должен удалиться первым
(так как файлу положено жить только одну минуту, а папкам две). Вам понадобятся библиотеки os и shutil.
Внимательно перечитайте задание и учтите возможные ошибки.
'''

# =======================DEL OLD FILES AND DIRS from list ================================================

# dir_mon = os.getcwd() + '/mon'  # Следим за изменениями в этой директории
#
# def old_data(dir_mon):
#     container = []  # список для последующей обработки
#
#     for i in os.walk(dir_mon):
#         container.append(i)  # сохраним в список для последующей многократной обработки
#
#     for root, dirs, files in container:
#         for file in files:
#             f = (root + '/' + file)
#             file_mod = datetime.datetime.fromtimestamp(os.path.getatime(f))
#             if datetime.datetime.now() - file_mod > datetime.timedelta(seconds=60):
#                 os.remove(f)
#
#         for dir in dirs:
#             d = (root + '/' + dir)
#             dir_mod = datetime.datetime.fromtimestamp(os.path.getatime(d))
#             if datetime.datetime.now() - dir_mod > datetime.timedelta(seconds=120):
#                 if len(os.listdir(d)) == 0:
#                     os.rmdir(d)
#                 else:
#                     pass
#         pass
#
# while True:
#     old_data(dir_mon)


# =======================DEL OLD FILES AND DIRS from os.path.join================================================

dir_mon = os.getcwd() + '/mon'  # Следим за изменениями в этой директории


def old_data_clearing(dir_mon):
    for root, dirs, files in os.walk(dir_mon):
        for file in files:
            f = os.path.join(root, file)
            file_mod = datetime.datetime.fromtimestamp(os.path.getatime(f))
            if datetime.datetime.now() - file_mod > datetime.timedelta(seconds=60):
                os.remove(f)

        for dir in dirs:
            d = os.path.join(root, dir)
            dir_mod = datetime.datetime.fromtimestamp(os.path.getatime(d))
            if datetime.datetime.now() - dir_mod > datetime.timedelta(seconds=120):
                if len(os.listdir(d)) == 0:
                    os.rmdir(d)
                else:
                    pass


while True:
    old_data_clearing(dir_mon)

# =======================EOF==================================================================================
