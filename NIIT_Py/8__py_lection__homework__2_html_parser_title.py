import urllib.request

'''
Парсинг (разбор) html страничек очень часто предлагают в качестве тестового задания для оценки способностей.
Напишите класс, который запрашивает содержимое веб-странички с помощью стандартной библиотеки urllib.request
и вытаскивает у нее заголовок (содержимое тэга title) любым придуманным вами способом
(с помощью стандартной или скаченной библиотеки, как угодно). Например,
если вы скачаете страничке по адресу "http://python.org/",
то можете найти у нее указанный тег и текст внутри него - "<title>Welcome to Python.org</title>"
'''

print('\n =======================HtmlParser==========================================================================')


class HtmlParser():
    '''HtmlParser'''

    def __init__(self, url):
        self.url = url

    def __str__(self):
        htmlpage = urllib.request.urlopen(self.url).read().decode('utf-8')
        title = str(htmlpage).split('<title>')[1].split('</title>')[0]
        return title


parser = HtmlParser('http://python.org')
print(parser)

print('\n =======================EOF=================================================================================')
