# Написать собственную реализацию встроенной функции маx.
# Она принимает любое количество аргументов и возвращает максимальный аргумент

print('--------------def max-----------------------------')

def maxxx (*args):
    one = args[0]
    for i in args[1:]:
        if i > one:
            one = i
    return one

s = maxxx(33, 44, 55, 42, 15, 17, 77, 68)
print(s)

print('--------------end def max-----------------------------\n')

# Написать собственную реализацию встроенной функции enumerate()
# применяется для итерируемых коллекций (строки, списки, словари и др.)
# и создает объект, который генерирует кортежи, состоящие из двух элементов - индекса элемента и самого элемента:

print('--------------enumerate-----------------------------')

list1 = [10, 20, 30, 40]
num = 0
for xxx in list1:
    num += 1
    print(num, xxx)

print('--------------end enumerate-----------------------------\n')

print('-----------add Dictionary----------------------------')

enemy = {}

list2 = [10, 20, 30, 40]
n = 0
for x in list2:
    n += 1
    enemy[n] = x

print(enemy)

print('-----------end add Dictionary----------------------------')