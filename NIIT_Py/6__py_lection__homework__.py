import os
import sys
import sh

# homework 6

'''
Реализацию встроенной функции len: функция принимает список, возвращает его длину
'''
print('=======================def len============================================================================== \n')

list1 = ('с', 'п', 'и', 'с', 'о', 'к')


def mylen(args):
    for num, i in enumerate(args):
        num += 1
    return num


print(mylen(list1))

print('=======================end def len========================================================================== \n')

'''
Реализацию функции reversed: ф-ия принимает список, возращает его, но элементы расположенны в обратном порядке
'''
print('=======================def reversed========================================================================= \n')

list2 = ('с', 'п', 'и', 'с', 'о', 'к')


def myreversed(args):
    li = []
    for i in range(1, mylen(list2) + 1):
        li.append(list2[mylen(list2) - i])
    return li


print(myreversed(list2))

print('=======================def to_title========================================================================= \n')
'''
ф-ию to_title: принимает строчку, ищет пробелы, первые буквы после них делает заглавными:
to_title("nikita Ladoshkin evgenyevich") == "Nikita Ladoshkin Evgenyevich"
'''

print('=======================def to_title========================================================================= \n')



print('=======================end def reversed========================================================================= \n')

'''
ф-ию copyfile: функция принимает источник и назначение, открывает источник, читает его, открывает назначение,
пишет в него. Если источника нет, то бросает ошибку, если назначение уже сущестсует, то бросает ошибку.
Проверить нужно и правильное выполнение ф-ии и выполнение при ошибке
'''
print('=======================copy file============================================================================ \n')

inputfile = '/home/sergi/Dev/less_python/penguinarium/NIIT_lesson/pass1.txt'
outputfile = '/home/sergi/Dev/less_python/penguinarium/NIIT_lesson/pass2.txt'


def copyfile(source, destination):
    if not os.path.exists(source):
        print('не удалось выполнить команду для {} : Нет такого файла или каталога'.format(source))
        return None
    try:
        with open(source, mode='r', encoding='latin-1') as file_r:
            for line in file_r:
                print(line.strip())
    except:
        Exception
    print(str(sys.argv[1:]))

    if not os.path.exists(destination):
        with open(destination, mode='w', encoding='latin-1') as file_w:
            file_w.write(source)
    else:
        print('невозможно создать Файл {} : Файл существует'.format(destination))


copyfile(inputfile, outputfile)

print('=======================end copy file======================================================================== \n')

print('=======================copy dir============================================================================= \n')

inputdir = (os.getcwd() + '/lib')
outputdir = (os.getcwd() + '/lib2')


def copydir(source, destination):
    if not os.path.exists(source):
        print('не удалось выполнить команду для {} : Нет такого файла или каталога'.format(source))
        return None
    if not os.path.exists(destination):
        path = source, destination
        sh.cp("-r", path)
        print('каталог {} успешно скопирован в {}'.format(source, destination))
    else:
        print('не удалось выполнить команду для {} : файл или каталог уже существуют'.format(destination))


copydir(inputdir, outputdir)

print('=======================end copy dir========================================================================= \n')

print('=======================User================================================================================= \n')


class User():
    """add name and age to user"""

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def setName(self):
        self.name = 'name'

    def getName(self):
        return self.name

    def setAge(self):
        self.age = 0

    def getAge(self):
        return self.age


class Worker(User):
    """set a salary to the worker"""

    def __init__(self, name, age, salary):
        self.salary = salary
        super().__init__(name, age)

    def setSalary(self):
        self.solary = 0

    def getSalary(self):
        return self.salary

    def getSumSalaryWorkers(self):
        return self.salary


id_001 = Worker('Иван', '25', '1000')
id_002 = Worker('Вася', '26', '2000')

print(int(id_001.getSalary()) + int(id_002.getSalary()))


class Student(User):
    """add a type of Student set a stipend and level"""

    def __init__(self, name, age, stipend, level):
        super.__init__(name, age)
        self.level = level
        self.stipend = stipend


class Driver(Worker):
    """add a type of working driver set a experience and category"""

    def __init__(self, name, age, salar):
        super.__init__(name, age, salar)
        self.experience = 0
        self.a = 'A'
        self.b = 'B'
        self.c = 'C'


print('=======================End User============================================================================= \n')

print('=======================End User============================================================================= \n')
"""
Реализовать свой класс-итератор, которых при итерации генерирует простые числа
(столько, сколько задали при создании экземпляра класса)
"""



print('=======================End User============================================================================= \n')