from subprocess import Popen, PIPE
import json
import os
import getpass
import socket
import datetime

'''
Используете subprocess библиотеку для того, чтобы читать содержимое файлов с помощью утилиты cat.
'''

print('\n =======================cat=================================================================================')

proc = Popen('cat /home/sergi/mic_setting', shell=True, stdout=PIPE, stderr=PIPE)

proc.wait()  # дождаться выполнения

res = proc.communicate()  # получить tuple('stdout', 'stderr')
if proc.returncode:
    print(res[1])
print('result:', res[0])

print('\n =======================END=================================================================================')
print('\n =======================HI PIPE test stdin, stdout and ping=================================================')

proces = Popen('cat', stdin=PIPE, stdout=PIPE)

print(
    proces.poll(),
    proces.stdin.write(b"Hi PEIPE "),
    proces.stdin.close(),
    proces.stdout.read(),
    proces.poll()
)
print('\n =======================test ping stdout====================================================================')
pinger=Popen('ping hmn.ru -c 4', shell=True, stdout=PIPE)
i = 0
while i < 5:
    i += 1
    s=pinger.stdout.readline().decode('cp866')
    print(datetime.datetime.now(), s.strip())

print('\n =======================END=================================================================================')

'''
Попробуйте библиотеку json, чтобы создать json файл, куда запишите текущую рабочую директорию (getcwd библиотеки os),
текущего user'a (getuser библиотеки getpass) и текущий хост (gethostname библиотеки socket)
'''
print('\n =======================json================================================================================')

user_name = getpass.getuser()
host_name = (socket.gethostbyname(socket.getfqdn()))
path_name = os.getcwd()

container = dict()

container['user'] = user_name
container['host'] = host_name
container['path'] = path_name

add_container = []
add_container.append(container)

jfile = path_name + '/' + user_name

# =======================DUMP JSON======================================

with open(jfile, mode='w') as file:
    json.dump(add_container, file)

# =======================LOAD JSON======================================

with open(jfile, mode='r', encoding='latin-1') as file2:
    jdata = json.load(file2)

    for users in jdata:
        print('Name is: ', str(users['user']))
        print('HostName is: ', str(users['host']))
        print('PathDir is: ', str(users['path']))

print('\n =======================EOF=================================================================================')