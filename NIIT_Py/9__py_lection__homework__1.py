import time
import threading


def odd_primes(start, end):
    list_primes = []
    for i in range(start, end):
        for a in range(2, i):
            if i % a == 0:
                break
        else:
            list_primes.append(i)
    return list_primes

start_time = time.time()

# prime_a = odd_primes(3, 1000)
# prime_b = odd_primes(1001, 2000)
# prime_c = odd_primes(2001, 3000)
#
# print(prime_a)
# print(prime_b)
# print(prime_c)

# print('последовательный запуск:', time.time() - start_time, "seconds")

start_time = time.time()

pool = []

for _ in range(3):
    th = threading.Thread(target=odd_primes, args=(3, 1000))
    th.start()
    pool.append(th)
    print(pool)

for th in pool:
    th.join()



print('параллельный запуск:', time.time() - start_time, "seconds")