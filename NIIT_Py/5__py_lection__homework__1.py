import itertools

list1 = ([1, 2, 3], [4, 5], [6, 7])

def a (*args):
    return list(itertools.chain.from_iterable(args))

print(a(*list1))

'''
Функция должна принимать три массива ([1, 2, 3], [4, 5], [6, 7]), а вернуть лишь один массив ([1, 2, 3, 4, 5, 6, 7])
'''

list2 = (['hello', 'i', 'write', 'cool', 'code'])



'''
Функция принимает массив (['hello', 'i', 'write', 'cool', 'code'])
и возвращать массив из элементов, у которых длина не меньше пяти (['hello', 'world'])
'''

def list3 (pas):
    return list(itertools.combinations(pas, 4))

print(list3('password'))

'''
Функция выдает на строку 'password' все возможные комбинации вида
([('p', 'a', 's', 's'), ('p', 'a', 's', 'w'), ('p', 'a', 's', 'o'), ...)
'''